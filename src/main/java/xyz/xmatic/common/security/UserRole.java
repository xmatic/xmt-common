package xyz.xmatic.common.security;

public enum UserRole {
    USER, ADMIN;
}
